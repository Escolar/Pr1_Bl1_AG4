#include <stdio.h>

int main() {
    printf("A  B  A||B\n");
    printf("1  0  %i\n", 1||0);
    printf("0  1  %i\n", 0||1);
    printf("1  1  %i\n", 1||1);
    printf("0  0  %i\n", 0||0);

    printf("\n");

    printf("A \t B \t A||B\n");
    printf("true \t false \t %s\n", 1||0 ? "true" : "false");
    printf("false \t true \t %s\n", 0||1 ? "true" : "false");
    printf("true \t true \t %s\n", 1||1 ? "true" : "false");
    printf("false \t false \t %s\n", 0||0 ? "true" : "false");

    return 0;
}